# BGTM - Engine for Copying Files and Directories

This is an engine which can be used in conjunction with
[BGTM](https://www.npmjs.com/package/bgtm) for
the purposes of copying files from one location to another.

# How to use

Read the how to section for [BGTM](https://www.npmjs.com/package/bgtm).

Run:

    npm install --save-dev bgtm-engine-copy

In your gulpfile add the following dependency:

    var copy = require('bgtm-engine-copy');

Then add the following task.

    tm.add('copy', {
        runOnBuild: true,
        watch: true,
        watchSource: [
            'src/copy/path1/**/*',
            'src/copy/path2/**/*',
            'src/copy/file1.html'
        ],
        liveReload: true,
        engine: copy,
        engineOptions: {
            'paths': [
                {
                    'src': 'src/copy/path1/**/*',
                    'dest': 'site/static/copy-path1/'
                },
                {
                    'src': 'src/copy/path2/**/*',
                    'dest': 'site/static/copy-path2/'
                },
                {
                    'src': 'src/copy/file1.html',
                    'dest': 'site/static/copy-file/'
                }
            ]
        }
    });

# What it does

* Gets the source files specified in the `src` property.
* Outputs them to the destination specified in the `dest` property.

# Engine Options

## Defaults

    {
        paths: []
    }

| Option       | Mandatory | Description |
|--------------|-----------|-------------|
| paths        | yes       | An array of objects defining the paths that should be copied. |

## Path Object

Each path object in the paths array should look like the following:

    {
        'src': 'src/copy/path1/**/*',
        'dest': 'site/static/copy-path1/'
    }

# License

Copyright 2017 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
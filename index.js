var deepExtend = require('deep-extend');
var fileSync = require('gulp-file-sync');
/**
 * This is just a simple copy runner for the BGTM engine, it does what it says on the box, copies from
 * one place to another.
 *
 * @param taskManager
 * @param args
 * @returns {*}
 */
module.exports = function (taskManager, args, done) {
    var defaults = {
        paths: []
    };

    var engineOptions = deepExtend({}, defaults, args);

    if (engineOptions.paths.length > 0) {
        for (var i in engineOptions.paths) {
            if (
                engineOptions.paths.hasOwnProperty(i)
                && typeof engineOptions.paths[i].src !== 'undefined'
                && typeof engineOptions.paths[i].dest !== 'undefined'
            ) {
                fileSync(engineOptions.paths[i].src, engineOptions.paths[i].dest);
            }
        }
    }
    done();
};